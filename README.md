# Задачи документа

Этот документ сделан для решения следующих задач

- Дать возможность каждому члену команды работать над любым проектом. Люди болеют, уходят в отпуска.
- Сократить время на ненужную коммуникацию в постоянно повторяющихся вопросах: как развернуть, как задеплоить, как посмотреть логи...
- Эффективно вводить новых людей в команду.
- Не использовать лес технологий и иснтрументов.


# Project Requirements

- Каждый проект содержит файл Readme.md, в котором должны быть описаны следующие пункты:
    - **Setup**.  Инструкция как развернуть проект на локальной машине. Этой информации должно быть достаточно, чтобы любой член команды самостоятельно и без вопросов развернул у себя на машине проект готовый к разработке.
    - **Deployment**. Инструкция как развернуть проект на staging и production серверах.
    - **Enviroment description**. Где посмотреть логи, на staging и production серверах, ...
- Архитектура должна быть расширяемой и готовой к постоянным изменениям (Agile, у нас требования постоянно менются). Стремится соблюдать принципы проектирования SOLID.
- Бизнес логика должна быть покрыта Unit test-ами (гораздо лучше использовать TDD, тогда тесты выйдут сами собой).
- Использовать kanban доску для трекинга задачи и багов. (kanbanflow.com)
- Для шаринга .Net Assembly между проектами использовать TruVisibility NuGet репозиторий

# Project Best Practices

- Предпочтительные технологии и связки для проектов без специальнрых требований:
    - (.Net core, angular, mongo/mysql, linux)
    - (Laravel php, angular, mysql, linux)
- Использование TDD при разработке бизнес логики.
- Интеграционные тесты на Api контролеры.
- Настройка CI сервиса для прогонки тестов


# .Net Naming Guides

## Общие рекомендации
- Для возвращаемых коллекций использовать IList<T>, если нет специальной необходимости использовать явный тип
- Если поиск по коллекции не дал результатов, то возврашать пустую коллекцию new T[0] (а не null).
- Для входных коллекций использовать меньший допустимый интерфейс т.е. IEnumerable<T> предпочтительнее, чем IList<T> (Count например)
- Если метод предпологает использование пейджинга то добавлять ему суффикс WithPaging. Все такие методы возвращают PageOf<T>
- Все методы доступа к данным по умолчанию async, ну нежно добавлять этот суффикс в имя метода


## Именование Api Clients, Repositories and other data sources
- Не нужно указывать имя сущности в методе, если она такая же как и сам репозиторий. То есть в IContactRepository метод получения контакта это GetById, а не ~~GetContactById~~.
- **Get** - Запрос определенных объектов. Если объекта нету, то возращаем null. (GetById, GetByIds)
- **Find** - Поиск объектов по какому-либо критерию (всегда возвращает список), может использовать сортировку и paging
- **Count** - Запрос количества объектов по какому-либо критерию
- **Create** - Создание
- **Update** - Обновление
- **Delete** - Удаление
- Наиболее популярные названия, которые стоит использовать: **GetById, GetByIds, FindByTerm, FindByTermWithPaging, Create, Update, Delete**


Примеры:
```csharp
// Get examples
Task<CrmContact> GetById(Guid id);
Task<CrmContact> GetByEmail(Guid accountId, string email);
Task<IList<CrmContact>> GetByIds(IList<Guid> ids);
Task<IList<CrmCustomField>> GetByAccount(Guid accountId);

// Find examples
Task<IList<CrmContact>> FindByBirthday(int day, int month);
Task<IList<Contact>> FindByTerm(Guid accountId, string searchTerm = null, ISortInfo sortInfo = null);
Task<PageOf<Contact>> FindByTermWithPaging(Guid accountId, string searchTerm = null, IPageSortInfo pageInfo = null); true, int pageNumber = 1, int pageSize = 100);

// Count
Task<long> CountByAccount(Guid accountId);
Task<long> CountByExpression(Expression<Func<Contact, bool>> expression);

// Modifications
Task<CrmContact> Create(CrmContact contact);
Task Update(CrmContact contact);
Task Delete(Guid id);
```

## Некотрые правила REST routings

```

// Get by id
GET /api/contacts/{id}

// Get by ids
POST /api/contacts/list

// search
GET /api/contacts?searchTerm=oleg&ordetBy=email&ascending=false

// search with paging
GET /api/contacts/paginate?searchTerm=oleg&ordetBy=email&ascending=false&pageNumber=1&pageSize=20

// create 
PUT /api/contacts

// update
POST /api/contacts

 

```